/*
package com.inductionhq.testdrivendevelopment.bowling;

public class GameSource {

    public class Game {
        public void roll(int pins) {
        }
    }

    public class Game {
        public void roll(int pins) {
        }

        public int score() {
            return -1;
        }
    }

    public class Game {
        private int score = 0;

        public void roll(int pins) {
            score += pins;
        }

        public int score() {
            return score;
        }
    }

    public class Game {
        private int score = 0;
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            score += pins;
            rolls[currentRoll++] = pins;
        }

        public int score() {
            return score;
        }
    }

    public class Game {
        private int score = 0;
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            score += pins;
            rolls[currentRoll++] = pins;
        }

        public int score() {
            int score = 0;
            for (int i = 0; i < rolls.length; i++)
                score += rolls[i];
            return score;
        }
    }

    public class Game {
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            rolls[currentRoll++] = pins;
        }

        public int score() {
            int score = 0;
            for (int i = 0; i < rolls.length; i++)
                score += rolls[i];
            return score;
        }
    }

    public class Game {
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            rolls[currentRoll++] = pins;
        }

        public int score() {
            int score = 0;
            for (int i = 0; i < rolls.length; i++)  {
                if (rolls[i] + rolls[i+1] == 10) // spare
                    score += ...
                score += rolls[i];
            }
            return score;
        }
    }

    public class Game {
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            rolls[currentRoll++] = pins;
        }

        public int score() {
            int score = 0;
            for (int i = 0; i < rolls.length; i++)
                score += rolls[i];
            return score;
        }
    }

    public class Game {
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            rolls[currentRoll++] = pins;
        }

        public int score() {
            int score = 0;
            int i = 0;
            for (int frame = 0; frame < 10; frame++)  {
                score += rolls[i] + rolls[i+1];
                i += 2;
            }
            return score;
        }
    }

    public class Game {
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            rolls[currentRoll++] = pins;
        }

        public int score() {
            int score = 0;
            int i = 0;
            for (int frame = 0; frame < 10; frame++) {
                if (rolls[i] + rolls[i + 1] == 10) // spare
                {
                    score += 10 + rolls[i + 2];
                    i += 2;
                } else {
                    score += rolls[i] + rolls[i + 1];
                    i += 2;
                }
            }
            return score;
        }
    }

    public class Game {
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            rolls[currentRoll++] = pins;
        }

        public int score() {
            int score = 0;
            int frameIndex = 0;
            for (int frame = 0; frame < 10; frame++) {
                if (rolls[frameIndex] +
                        rolls[frameIndex + 1] == 10) // spare
                {
                    score += 10 + rolls[frameIndex + 2];
                    frameIndex += 2;
                } else {
                    score += rolls[frameIndex] +
                            rolls[frameIndex + 1];
                    frameIndex += 2;
                }
            }
            return score;
        }
    }

    public class Game {
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            rolls[currentRoll++] = pins;
        }

        public int score() {
            int score = 0;
            int frameIndex = 0;
            for (int frame = 0; frame < 10; frame++) {
                if (isSpare(frameIndex))
                {
                    score += 10 + rolls[frameIndex + 2];
                    frameIndex += 2;
                } else {
                    score += rolls[frameIndex] +
                            rolls[frameIndex + 1];
                    frameIndex += 2;
                }
            }
            return score;
        }

        private boolean isSpare(int frameIndex) {
            return rolls[frameIndex] +
                    rolls[frameIndex + 1] == 10;
        }
    }

    public class Game {
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            rolls[currentRoll++] = pins;
        }

        public int score() {
            int score = 0;
            int frameIndex = 0;
            for (int frame = 0; frame < 10; frame++) {
                if (rolls[frameIndex] == 10) // strike
                {
                    score += 10 +
                            rolls[frameIndex+1] +
                            rolls[frameIndex+2];
                    frameIndex++;
                }
                else if (isSpare(frameIndex))
                {
                    score += 10 + rolls[frameIndex + 2];
                    frameIndex += 2;
                } else {
                    score += rolls[frameIndex] +
                            rolls[frameIndex + 1];
                    frameIndex += 2;
                }
            }
            return score;
        }

        private boolean isSpare(int frameIndex) {
            return rolls[frameIndex] +
                    rolls[frameIndex + 1] == 10;
        }
    }

    public class Game {
        private int rolls[] = new int[21];
        private int currentRoll = 0;

        public void roll(int pins) {
            rolls[currentRoll++] = pins;
        }

        public int score() {
            int score = 0;
            int frameIndex = 0;
            for (int frame = 0; frame < 10; frame++) {
                if (isStrike(frameIndex)) {
                    score += 10 + strikeBonus(frameIndex);
                    frameIndex++;
                } else if (isSpare(frameIndex)) {
                    score += 10 + spareBonus(frameIndex);
                    frameIndex += 2;
                } else {
                    score += sumOfBallsInFrame(frameIndex);
                    frameIndex += 2;
                }
            }
            return score;
        }

        private boolean isStrike(int frameIndex) {
            return rolls[frameIndex] == 10;
        }

        private int sumOfBallsInFrame(int frameIndex) {
            return rolls[frameIndex] + rolls[frameIndex+1];
        }

        private int spareBonus(int frameIndex) {
            return rolls[frameIndex+2];
        }

        private int strikeBonus(int frameIndex) {
            return rolls[frameIndex+1] + rolls[frameIndex+2];
        }

        private boolean isSpare(int frameIndex) {
            return rolls[frameIndex]+rolls[frameIndex+1] == 10;
        }
    }


}
*/
