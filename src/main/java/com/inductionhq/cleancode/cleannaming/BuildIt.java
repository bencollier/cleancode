package com.inductionhq.cleancode.cleannaming;

/* This class, and the package and repository containing it are Copyright 2019 Induction Ltd.
All rights reserved. This code may not be copied or duplicated without the express permission of the copyright holder.
*/

/*
This class represents a Point of Sale order. The programmer who wrote it was not following best practices in naming
things.
 */

public class BuildIt {

    private static final double RATE = 0.20;

    private String hisName;
    private double m_priceAfterTax;
    private String[] sThingList;
    private int b = 0;

    public BuildIt(String hisName, int cost, String thing1, String thing2, String thing3) {
        this.hisName = hisName;
        this.m_priceAfterTax = cost * RATE;
        sThingList = new String[] {thing1, thing2, thing3};
        this.b = 0;
    }

    public void boom() {
        int i = 0;
        System.out.println(String.format("Customer: %s",this.hisName));
        for (String thing: this.sThingList) {
            System.out.println(i + ":");
            System.out.println(thing);
            i = i + 1;
        }
        System.out.println(String.format("Price including VAT: %s", this.m_priceAfterTax));
        this.b++;
        System.out.println(String.format("Invoice printing number %s", this.b));
    }

}
