package com.inductionhq.cleancode.cleanfunctions;

import com.inductionhq.helpers.DocumentAPI;

import java.math.BigDecimal;

public class Invoice {

    private boolean printed;
    private String invoiceDocument;

    public Invoice() {
        printed = false;
        invoiceDocument = "";
    }

    public String structure(boolean isTaxable, String[] lineItems, String customerName, String[] customerAddress,
                                 BigDecimal total, int tax) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Customer Name: " + customerName + "<br/>");
        stringBuilder.append("Address: " + customerAddress + "<br/>");
        if (isTaxable) {
            for(String lineItem: lineItems) {
                stringBuilder.append(lineItem);
            }
            total = total.multiply(new BigDecimal(tax));
        } else {
            for (String lineItem: lineItems) {
                stringBuilder.append(lineItem);
            }
        }
        stringBuilder.append("Total: " + total);
        this.printed = true;
        invoiceDocument = stringBuilder.toString();
        DocumentAPI documentAPI = new DocumentAPI();
        documentAPI.send(invoiceDocument);
        return invoiceDocument;
    }

}
